{ ... }:
{
  nixpkgs.hostPlatform = "x86_64-linux";
  time.timeZone = "Europe/Paris";

  networking = {
    hostName = "shuttle-vm1";
    useDHCP = true;
  };

  boot.kernelModules = ["kvm-intel"];

  environment.sessionVariables = {
    XKB_DEFAULT_LAYOUT = "us,us";
    XKB_DEFAULT_VARIANT = "altgr-intl,dvorak-alt-intl";
    XKB_DEFAULT_OPTIONS = "grp:alt_shift_toggle";
  };

  imports = [
    ../specs/common.nix
    ../specs/swap.nix
    ../specs/qemu.nix
    ../specs/endpoint.nix
    ../specs/uefi.nix

    ../users/sowlipsism
  ];
}
