{ ... }:
{
  nixpkgs.hostPlatform = "x86_64-linux";
  time.timeZone = "Europe/Paris";

  networking = {
    hostName = "shuttle";
    useDHCP = true;
  };

  hardware.cpu.intel.updateMicrocode = true;
  boot.kernelModules = ["kvm-intel"];
  boot.loader.efi.canTouchEfiVariables = false;

  # This is pretty much only used by cage to show the greeter. The console
  # itself only allows for QWERTY, but I don't mind not having Dvorak on the
  # rare occasion I do go there. User-level programs really should not rely on
  # those being set if they can help it.
  environment.sessionVariables = {
    XKB_DEFAULT_LAYOUT = "us,us";
    XKB_DEFAULT_VARIANT = "altgr-intl,dvorak-alt-intl";
    XKB_DEFAULT_OPTIONS = "grp:alt_shift_toggle";
  };

  imports = [
    ../specs/common.nix
    ../specs/laptop.nix
    ../specs/endpoint.nix
    ../specs/uefi.nix

    ../programs/greeter.nix

    ../users/sowlipsism
  ];
}
