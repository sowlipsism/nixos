{ ... }:
{
  system.stateVersion = "23.05";

  # Enforce XDG paths at system level, otherwise user systemd does
  # not find Home Manager units.
  environment.sessionVariables = {
    XDG_CONFIG_HOME = "$HOME/.local/etc";
    XDG_DATA_HOME = "$HOME/.local/share";
    XDG_STATE_HOME = "$HOME/.local/var/state";
    XDG_CACHE_HOME = "$HOME/.local/var/cache";
  };

  nix = {
    settings.experimental-features = ["nix-command" "flakes"];
    extraOptions = ''
      use-xdg-base-directories = true
    '';
  };

  programs = {
    fish.enable = true;
    git.enable = true;
  };

  fileSystems."/" = {
    device = "/dev/disk/by-partlabel/nixos-root";
    fsType = "ext4";
  };

  home-manager = {
    useUserPackages = true;
    useGlobalPkgs = true;
  };
}
