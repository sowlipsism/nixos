{ pkgs, ... }:
{
  imports = [
    ../programs/greeter.nix
    ../programs/generic-session.nix
  ];

  # Graphics
  hardware.opengl.enable = true;
  boot.plymouth.enable = true;
  programs.generic-session.enable = true;
  services.greetd.enable = true;
  environment.etc."greetd/environments".text = ''
    start graphical session
    start shell
  '';

  # Audio
  services.pipewire = {
    enable = true;
    alsa.enable = true;
    jack.enable = true;
    pulse.enable = true;
  };

  # Security and Identity
  security.polkit.enable = true;
  services.gnome.gnome-keyring.enable = true;
  services.pcscd.enable = true;
  services.udev.packages = [ pkgs.yubikey-personalization ];
}
