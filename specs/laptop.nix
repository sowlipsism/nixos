{ modulesPath, ... }:
{
  imports = [
    (modulesPath + "/installer/scan/not-detected.nix")
  ];

  boot.initrd.availableKernelModules = [
    "xhci_pci" "thunderbolt" "nvme" "uas" "sd_mod"
  ];

  powerManagement.cpuFreqGovernor = "powersave";
  networking.wireless.iwd.enable = true;
}
