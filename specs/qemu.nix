{ modulesPath, ... }:
{
  imports = [
    "${modulesPath}/profiles/qemu-guest.nix"
  ];

  boot.initrd.availableKernelModules = [
    "ahci" "xhci_pci" "virtio_pci" "sr_mod" "virtio_blk"
  ];

  fileSystems."/etc/nixos" = {
    device = "nixos-config";
    fsType = "virtiofs";
  };

  services.openssh.enable = true;
}
