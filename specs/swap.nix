{ ... }:
{
  swapDevices = [
    { device = "/dev/disk/by-partlabel/nixos-swap"; }
  ];
}
