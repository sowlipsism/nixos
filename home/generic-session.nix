{ config, lib, pkgs, ... }:
let
  cfg = config.programs.generic-session;
in {
  options.programs.generic-session = {
    enable = lib.mkEnableOption "generic session user script";
    environment = lib.mkOption {
      type = lib.types.bool;
      default = true;
      example = false;
      description = ''
        Whether the session script should source environment variables defined
        by Home Manager.
      '';
    };

    default = lib.mkOption {
      type = lib.types.str;
      default = "shell";
      example = "graphical";
      description = ''
        The session that should be invoked, if defined by the user, when the
        script is called with an unknown parameter.
      '';
    };

    entrypoints = lib.mkOption {
      type = lib.types.attrsOf lib.types.str;
      default = {
        shell = "$SHELL";
      };
      example = lib.literalExpression ''
        {
          graphical = "sway";
          shell = "zsh";
        }
      '';
      description = ''
        Maps a generic named entrypoint to a user-specific command. These
        entrypoints can then be made available to your greeter so that users may
        choose their own graphical environment.
      '';
    };
  };

  config = lib.mkIf cfg.enable {
    home.packages = [
      (pkgs.writeShellScriptBin "start-session" ''
        ${if cfg.environment then "source ${config.home.sessionVariablesPackage}/etc/profile.d/hm-session-vars.sh" else ""}
        case $1 in
          ${lib.strings.concatStringsSep "\n  " (builtins.attrValues (builtins.mapAttrs (name: value: "${name}) exec ${value};;") cfg.entrypoints))}
          *) exec ${cfg.entrypoints.${cfg.default}};;
        esac
      '')
    ];
  };
}
