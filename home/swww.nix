{ config, lib, pkgs, ... }:
let
  cfg = config.services.swww;
in {
  options.services.swww = {
    enable = lib.mkEnableOption "swww - a fancy wallpaper manager";
    package = lib.mkPackageOption pkgs "swww" {};
    wantedBy = lib.mkOption {
      type = lib.types.str;
      default = "graphical-session.target";
      example = "sway-session.target";
      description = ''
        The systemd target that should pull in the swww daemon.
      '';
    };
  };

  config = lib.mkIf cfg.enable {
    assertions = [
      (lib.hm.assertions.assertPlatform "services.swww" pkgs lib.platforms.linux)
    ];

    home.packages = [cfg.package];
    systemd.user.services.swww = {
      Unit = {
        Description = "swww wallpaper daemon";
        PartOf = ["graphical-session.target"];
        After = ["graphical-session.target"];
      };

      Service = {
        ExecStart = "${cfg.package}/bin/swww-daemon";
        Restart = "on-failure";
        KillMode = "mixed";
      };

      Install = {
        WantedBy = [cfg.wantedBy];
      };
    };
  };
}
