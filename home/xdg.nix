{ config, ... }:
let
  homeDirectory = "/home/${config.home.username}";
in {
  home = {
    inherit homeDirectory;
    sessionPath = ["${homeDirectory}/.local/bin"];
  };

  nix.settings = {
    use-xdg-base-directories = true;
  };

  xdg = {
    enable = true;
    cacheHome = "${homeDirectory}/.local/var/cache";
    stateHome = "${homeDirectory}/.local/var/state";
    configHome = "${homeDirectory}/.local/etc";
    dataHome = "${homeDirectory}/.local/share";

    userDirs = {
      enable = true;
      createDirectories = true;
    };
  };
}
