{ config, pkgs, ... }:
{
  home.packages = [
    pkgs.pcscliteWithPolkit
  ];

  programs.gpg = {
    enable = true;
    homedir = "${config.xdg.dataHome}/gnupg";
    scdaemonSettings = {
      pcsc-driver = "${pkgs.pcscliteWithPolkit.out}/lib/libpcsclite.so";
      card-timeout = "5";
      disable-ccid = true;
      pcsc-shared = true;
    };
  };

  services.gpg-agent = {
    enable = true;
    enableExtraSocket = true;
    enableSshSupport = true;
    pinentryFlavor = "gnome3";
  };
}
