{ config, lib, pkgs, ... }:
let
  cfg = config.programs.generic-session;
in {
  options.programs.generic-session = {
    enable = lib.mkEnableOption "generic session entrypoint";
  };

  config.environment.systemPackages =  lib.mkIf cfg.enable [
    (pkgs.writeShellScriptBin "start" ''
      if which start-session > /dev/null 2> /dev/null; then
        start-session "$@"
      else
        printf 'User does not have a start-session script, ensure that your Home Manager configuration has option \e[33mprograms.generic-session.enable\e[0m set to \e[35mtrue\e[0m.\n'
        exec $SHELL
      fi
    '')
  ];
}
