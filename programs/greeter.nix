{ pkgs, ... }:
let
  stylesheet = pkgs.writeText "gtkgreet.css" ''
    window {
      background-color: #000000;
      font-family: "Iosevka", sans-serif;
      font-size: 14px;
    }

    box#body label, #clock, arrow {
      color: #FFFFFF;
    }

    #clock {
      margin: 2em 0;
    }

    box#body entry, box#body button {
      background: none;
      border: none;
      color: #FFFFFF;
      border-bottom: solid 1px #FFFFFF;
      border-radius: 0;
      box-shadow: none;
      margin: 0.5em 0;
      padding: 0.5em 0.5em;
      caret-color: #FFFFFF;
    }
    box#body entry:focus, box#body button:hover, #gtk-combobox-popup-menu :hover {
      background: rgba(255, 255, 255, 0.25);
      color: #FFFFFF;
    }

    box#body .text-button {
      padding: 0.5em 1em;
    }

    box#body .suggested-action {
      border-bottom: solid 1px #7EBAE4;
    }

    box#body .suggested-action:hover {
      background: rgba(126, 186, 228, 0.25);
    }
  '';
in {
  fonts.packages = [pkgs.iosevka];

  services.greetd.settings.default_session = {
    command = "${pkgs.cage}/bin/cage -d -m last -s -- ${pkgs.greetd.gtkgreet}/bin/gtkgreet -l -s ${stylesheet}";
  };

  users.users.greeter = {
    createHome = true;
    home = "/var/lib/greetd";
  };
}
