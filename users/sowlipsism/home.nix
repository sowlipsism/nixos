{ config, pkgs, ... }:
{
  home.username = "sowlipsism";
  imports = [
    ../../home/generic-session.nix
    ../../home/gnupg.nix
    ../../home/swww.nix
    ../../home/xdg.nix
  ];

  services = {
    swww.enable = true;
  };

  programs.generic-session = {
    enable = true;
    default = "graphical";
    entrypoints = {
      graphical = "Hyprland";
      shell = "fish";
    };
  };

  nix.registry = {
    xdg-ninja = {
      from = {
        id = "xdg-ninja";
        type = "indirect";
      };
      to = {
        owner = "b3nj5m1n";
        repo = "xdg-ninja";
        type = "github";
      };
    };
  };

  home.packages = [
    # pkgs.hello
    # (pkgs.nerdfonts.override { fonts = [ "FantasqueSansMono" ]; })
    # (pkgs.writeShellScriptBin "my-hello" ''
    #   echo "Hello, ${config.home.username}!"
    # '')
  ];

  home.file = {
    # ".screenrc".source = dotfiles/screenrc;
    # ".gradle/gradle.properties".text = ''
    #   org.gradle.console=verbose
    #   org.gradle.daemon.idletimeout=3600000
    # '';
  };

  home.sessionVariables = {
    EDITOR = "kak";
  };

  programs = {
    home-manager.enable = true;
    broot.enable = true;
    firefox.enable = true;
    fish.enable = true;
    jq.enable = true;
    kakoune.enable = true;
    kitty.enable = true;
    ripgrep.enable = true;
    tmux.enable = true;
    eza = {
      enable = true;
      git = true;
    };
  };

  wayland.windowManager.hyprland = {
    enable = true;
  };

  services.gpg-agent.sshKeys = [ "05D8C8844E1BF7FF21C9B7C838E8B5739CD9F3BD" ];
  programs.gpg.publicKeys = [
    { source = ./public.asc; trust = "ultimate"; }
  ];

  # This value determines the Home Manager release that your configuration is
  # compatible with. This helps avoid breakage when a new Home Manager release
  # introduces backwards incompatible changes.
  #
  # You should not change this value, even if you update Home Manager. If you do
  # want to update the value, then make sure to first check the Home Manager
  # release notes.
  home.stateVersion = "23.05"; # Please read the comment before changing.
}
