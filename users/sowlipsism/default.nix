{ pkgs, ... }:
{
  users.users.sowlipsism = {
    isNormalUser = true;
    extraGroups = ["wheel"];
    shell = pkgs.fish;
  };

  home-manager.users.sowlipsism = import ./home.nix;
}
