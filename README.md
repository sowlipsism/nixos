sowlipsism's Nix configurations
===============================

This flake exposes both my NixOS and Home Manager configurations!

Repository Structure
--------------------

Configuration is spread in various directories, which are as follows:

### `hosts`

These files correspond to the main configurations declared in `flake.nix`,
each host should mostly include imports of other files from the `specs`, `users`
and `programs` directories.

### `users`

Each user has their own directory, in which two main files are expected to be
present:
 - `default.nix` is the file loaded by NixOS configurations to create and
   configure the user
 - `home.nix` is the main entrypoint for Home Manager configuration

Any configuration specific to a single user should remain in their directory,
unless said configuration is found to be common amongst all users.

### `programs`

These files configure individual programs or services which require more than
a few lines to enable. They're usually imported by hosts.

### `home`

These files configure individual programs or services which require more than
a few lines to enable. They're usually imported by users' Home Manager
configurations.

### `specs`: Machine specifications

This directory contains short snippets that relate to a specific feature
that is or is not available on the current machine. For now, the following
are available:
 - `common.nix` defines defaults that are expected on all machines
 - `uefi.nix` mounts the ESP on /boot and enables systemd-boot
 - `swap.nix` enables the use of a swap partition
 - `endpoint.nix` enables plymouth, polkit and other desktop-centric services
 - `laptop.nix` enables iwd, Thunderbolt and other features expected on a laptop
 - `qemu.nix` loads the virtual machine profile and mounts a virtiofs bind at
   `/etc/nixos` for easy testing of configuration changes
